/*
 * weather_station.h
 *
 *  Created on: Dec 1, 2020
 *      Author: akshay
 */

#ifndef CORE_INC_HW__weather_station_H_
#define CORE_INC_HW__weather_station_H_
#ifdef __cplusplus
extern "C" {
#endif


#define BATT_POWER    	1
#define WINDDIR_POWER   4
#define PRESSURE_POWER  2
#define TEMPHUM_POWER	3
#define WINDSPEED_POWER 5

#define DOWNLINK_RAINPORT        	5

typedef struct {
	uint16_t pressure; /* in mbar */

	uint16_t temperature; /* in �C   */

	uint16_t humidity; /* in %    */

	uint16_t windDirection;

	uint16_t rainfall;

	uint16_t windSpeed;

	uint16_t batteryLevel;

} awsSensors_t;




void onWindSpeedTimerEvent();
void rainGaugeInterruptEnable();
void windSpeedInterruptEnable();

void windSpeedTimerEvent();
void weatherStationInit();
void readWeatherStationParameters(awsSensors_t *sensor_data);
void weatherStationGPIO_Init();
uint16_t getTotalRainfall(uint8_t downlinkReceived);

#ifdef __cplusplus
}
#endif

#endif /* CORE_INC_HW_weather_station_H_ */
